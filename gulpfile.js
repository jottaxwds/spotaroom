var gulp = require("gulp");
var sass = require("gulp-sass");
var concat = require("gulp-concat");
var sassFiles = "./src/**/*.scss";
function styles() {
  return gulp
    .src(sassFiles, {
      sourcemaps: true
    })
    .pipe(sass())
    .pipe(
      gulp.dest(function (file) {
        return file.base;
      })
    )
    .pipe(concat("App.css"))
    .pipe(gulp.dest("./src"));
}

//Watch task
gulp.task("watch", function () {
  gulp.watch(sassFiles, styles);
});
