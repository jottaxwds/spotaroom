const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const cors = require("cors");

const homecards = require("./routes/api/homecards");

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use("/api/homecards", homecards);

const port = 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
