const filterIds = idsObj => idsObj.filter((info, index) =>
  index < 30 && info.id
);

const createQueryIds = idsObj =>
  idsObj.length > 0
    ? idsObj.reduce((current, item, index) => {
      return index !== 0
        ? `${current}&ids[]=${item.id}`
        : `ids[]=${item.id}`;
    }, "")
    : "";


const formatHomecards = homecards => homecards.map(({
  id,
  type,
  title,
  photoUrls: { homecard: thumbnail },
  pricePerMonth: price,
  currencySymbol: currency,
  city
}) => {
  return {
    id,
    type,
    title,
    thumbnail,
    price,
    currency,
    city
  };
});


module.exports.filterIds = filterIds;
module.exports.createQueryIds = createQueryIds;
module.exports.formatHomecards = formatHomecards;