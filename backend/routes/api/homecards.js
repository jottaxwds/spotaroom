const express = require("express");
const request = require("request");
const paths = require("./../../config/paths");
const utils = require("../../utils/utils")
const router = express.Router();

// @route   GET api/homecards
// @desc    Test
// @access  Public
router.get("/", (req, res) => {
  const locationParam = req.query.city || paths.DEFAULT_LOCATION;
  request(
    `${paths.BASE_URL}${paths.MARKERS}${locationParam}`,
    (err, resp, body) => requestCallback(res, err, resp, body)
  );
});


const requestCallback = async (res, err, resp, body) => {
  if (err !== null) {
    res.status(500).json("Error retreiving ids for that location", err);
  }
  // Retreived data:
  const response = JSON.parse(body);

  // Parsing IDS to build query:
  if (response && response.data && response.data.length > 0) {
    let filteredIds = utils.filterIds(response.data)
    let queryIds = utils.createQueryIds(filteredIds)

    // If valid query, get homecards:
    if (queryIds !== "") {
      // homecards
      let homecards = await request(
        `${paths.BASE_URL}${paths.HOMECARDS}${queryIds}`,
        (err, resp, body) => homeCardsCallback(res, err, resp, body)
      );
    } else {
      res.status(500).json("Error parsing data from ids search", err);
    }
  } else {
    res.status(500).json("Error parsing data from app", err);
  }
}


const homeCardsCallback = (res, err, resp, body) => {
  try {
    let homecardsResponse = JSON.parse(body);
    let homecards = homecardsResponse.data.homecards;
    if (homecards && homecards.length > 0) {
      let formattedHomecards = utils.formatHomecards(homecards)
      return res.json(formattedHomecards);
    } else {
      return res.json([]);
    }
  } catch (err) {
    return res.status(500).json("Error parsing homecards", err);
  }
}

module.exports = router;
