module.exports = {
  BASE_URL: "https://www.spotahome.com/api/public/listings/search/",
  MARKERS: "markers/",
  HOMECARDS: "homecards_ids?",
  DEFAULT_LOCATION: "madrid"
};
