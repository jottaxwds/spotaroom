# Instructions to use/setup:

-> Clone the repo
-> In the root folder run following commands:

- `npm i`. It will install dependencies needed.
- `npm run dev` .  It will start express server used as a proxy for localhost:5000 and the FE server for react application in localhost:3000.


# Technical Approach:

## Frontend:
"create-react-app" boilerplate code was used as a basic structure for the Frontend.

Started by creating simple components to be able to re-use them in the application.
 - Navbar
 - Button
 - LoadingSpinner
 - HomeCard (to display info about properties)

Each component is inside "components" folder and inside it own folder with same name as the component. That folder contains also the styles and the unit tests.
src -> components -> Button:
 - `Button.js` (component js)
 - `Button.test.js` (unit tests)
 - `Button.scss `
 - `Button.css`


Style colors are set in a global sass file to access them through other styles used in components and layouts.

Simple gulp task is configured to compile the sass code into css and bundled in a single App.css file.

_(not needed to run the application)_ To run the gulp task and compile styles, in the root folder of the project run the following command:
-> `npm run styles`

## Backend:
A express server with basic router configuration (to handle future multiple routes/api paths and scale the app) is used.
IDS are retrieved from spotahome service and 30 first items are used to get the details for each property ID.


# Possible improvements:

 - EndToEnd tests could be fine.
 - Storybook to see the components from the UI perspective.
 - Lazy loading for images
 - Add support for more languages (by using i18n or other similar approach).
 - Error page depending on the error received by backend (better error handling).