import React from 'react';
import './Navbar.css';

const Navbar = (props) => {

  return <nav className="navbar__element">
    <div className="navbar__logo">
      <h1>SPOTAROOM</h1>
    </div>
    <div className="navbar__menu">
      <a href="#">The company</a>
      <a href="#">How we work</a>
      <a href="#">Contact us</a>
    </div>
  </nav>;
}

export default Navbar;