import React from "react";
import { shallow } from "enzyme";
import Navbar from "./Navbar";

describe("Navbar unit testing", () => {

  const logoUrl = "logoUrl.jpg";

  it("Should render", () => {
    const wrapper = shallow(<Navbar logo={logoUrl} />);
    expect(wrapper.find('.navbar__element').length).toEqual(1);
  });

});