import React from 'react';
import PropTypes from 'prop-types';

import Button from './../Button/Button';

const HomeCard = (props) => {
  const { propertyData } = props;
  let background = {
    backgroundImage: `url('${propertyData.thumbnail}')`
  };
  return (
    <li className="home__card--item">
      <div className="home__card--thumbnail" style={background}>
      </div>
      <div className="home__card--description">
        <div className="home__card--main">
          <div className="home__card--body">
            {propertyData.title}
          </div>
          <div className="home__card--price">
            <span>{propertyData.price} {propertyData.currency}</span>
          </div>
        </div>
        <div className="home__card--actionButtons">
          <Button variant="primary">
            More details
                </Button>
          <Button variant="secondary">
            Book now!
                </Button>
        </div>
      </div>

    </li>
  )
}

HomeCard.propTypes = {
  propertyData: PropTypes.object,
  sortType: PropTypes.string,
  properType: PropTypes.string
};

export default HomeCard;