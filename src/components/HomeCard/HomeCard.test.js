import React from "react";
import { shallow } from "enzyme";
import HomeCard from "./HomeCard";

describe("HomeCard unit testing", () => {
  const propertyData = {
    thumbnail: "myUrl",
    title: "description",
    price: "100",
    currency: "€"
  };

  const props = {
    propertyData: propertyData
  }

  it("Should render", () => {
    const wrapper = shallow(<HomeCard {...props} />);
    expect(wrapper.find('.home__card--item').length).toEqual(1);
    expect(wrapper.find('.home__card--body').text()).toEqual(propertyData.title);
    expect(wrapper.find('.home__card--price').text()).toEqual(`${propertyData.price} €`);
    expect(wrapper.find('Button').length).toEqual(2);
  });

  it("Should apply right background parameters", () => {
    const expected = { "backgroundImage": "url('" + propertyData.thumbnail + "')" };
    const wrapper = shallow(<HomeCard {...props} />);
    expect(wrapper.find('.home__card--item').length).toEqual(1);
    expect(wrapper.find('.home__card--thumbnail').prop('style')).toEqual(expected);
  });
});