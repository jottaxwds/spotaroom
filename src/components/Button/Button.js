import React from 'react';
import PropTypes from "prop-types";
import './Button.css';

const Button = (props) => {
  const { classNames, variant, onClick } = props;
  let extraClass = classNames && classNames.length > 0 ? classNames.join(" ") : "";
  return <button className={`button ${variant ? "button__" + variant : "button__default"} ${extraClass}`}
    onClick={onClick}>{props.children}</button>;
}

Button.propTypes = {
  onClick: PropTypes.func,
  classNames: PropTypes.array,
  variant: PropTypes.string
};
export default Button;