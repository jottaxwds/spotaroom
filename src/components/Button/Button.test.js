import React from "react";
import { shallow } from "enzyme";
import Button from "./Button";

describe("Button unit testing", () => {

  it("Should render", () => {
    const wrapper = shallow(<Button>Hello</Button>)
    expect(wrapper.find('.button__default').length).toEqual(1);
    expect(wrapper.find('.button__default').text()).toEqual("Hello");
  });

  it("Should take extraClasses", () => {
    const extraClasses = ["brown", "yellow"];
    const wrapper = shallow(<Button classNames={extraClasses}>Hello</Button>)
    expect(wrapper.find('.button__default').length).toEqual(1);
    expect(wrapper.find('.brown').length).toEqual(1);
    expect(wrapper.find('.yellow').length).toEqual(1);
  });

  it("Should apply variants", () => {
    const extraClasses = ["brown", "yellow"];
    const wrapper = shallow(<Button variant="danger" classNames={extraClasses}>Hello</Button>)
    expect(wrapper.find('.button').length).toEqual(1);
    expect(wrapper.find('.button__danger').length).toEqual(1);
  });

});