import React from 'react';
import PropTypes from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './LoadingSpinner.css';

const LoadingSpinner = (props) => {
  const { loadingMessage } = props;
  return <div className="loading__wrapper">
    <FontAwesomeIcon pulse={true} icon="spinner" />
    <span>{loadingMessage}</span>
  </div>;
}

LoadingSpinner.propTypes = {
  loadingMessage: PropTypes.string
}

export default LoadingSpinner;