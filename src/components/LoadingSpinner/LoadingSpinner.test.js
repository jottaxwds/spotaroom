import React from "react";
import { shallow } from "enzyme";
import LoadingSpinner from "./LoadingSpinner";

describe("LoadingSpinner unit testing", () => {
  const loadingMessage = "Loading";
  it("Should render", () => {
    const wrapper = shallow(<LoadingSpinner loadingMessage={loadingMessage} />);
    expect(wrapper.find('FontAwesomeIcon').length).toEqual(1);
    expect(wrapper.find('.loading__wrapper span').text()).toEqual(loadingMessage);
  });
});