import React, { useState, useEffect } from "react";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { getProperties } from "./API/APIHelpers";

import Navbar from "./components/Navbar/Navbar";
import HomeCard from "./components/HomeCard/HomeCard";
import LoadingSpinner from "./components/LoadingSpinner/LoadingSpinner";

import "./App.css";

const App = props => {
  library.add(faSpinner);
  const fontFamily = "Luckiest Guy";

  const [properties, setProperties] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    console.log("<useEffect> -> It works!");
    getProperties()
      .then(body => body.json())
      .then(properties => {
        updateStateWithFetchedProperties(properties);
      })
      .catch(err => {
        console.log("Error..");
        setIsLoading(false);
      });
  });

  const updateStateWithFetchedProperties = properties => {
    setIsLoading(false);
    setProperties(properties);
  };

  return (
    <div className="App">
      <Navbar />
      <div className="HomeCards__listing">
        {isLoading ? (
          <LoadingSpinner loadingMessage={"Plase wait..."} />
        ) : properties && properties.length > 0 ? (
          <ul className="HomeCards__listing--list">
            {properties.map((property, index) => {
              return <HomeCard key={`prop_${index}`} propertyData={property} />;
            })}
          </ul>
        ) : (
          <p style={{ fontFamily }}> Sorry, propertites not found... </p>
        )}
      </div>
    </div>
  );
};

export default App;
