import { API_BASEPATH } from "./paths";

export const getProperties = () => {
  return fetch(
    `${API_BASEPATH}`
  );
};
